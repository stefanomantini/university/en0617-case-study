
/*
 * Constants For System
 * 
 * Contains constants that relate to each part of the system 
 * 
 * 2016
 */
enum{
     emergencyShutdown = 4,
     error,
     resetSystem,
     pauseSystem,
     unPauseSystem,
     systemStart,
     systemStop,
     pad1Active,
     pad1Empty,
     pad2Active,
     pad2Empty,
     convReady,
     convStart,
     convSensor1Active,
     convSensor1Empty,
     conveyorActive,
     conveyorStationary,
     conveyorPaused,
     convSensor2Active,
     convSensor2Empty,
     robot1Start,
     robot1Stop,
     robot1Ready,
     robot1ReadyToPickup,
     robot1Moving,
     robot1ReadyToDropoff,
     robot1Shutdown,
     robot1Pause,
     robot1UnPause,
     robot2Start,
     robot2Stop,
     robot2Ready,
     robot2ReadyToPickup,
     robot2Moving,
     robot2ReadyToDropoff,
     robot2Shutdown,
     robot2Pause,
     robot2UnPause,
};
