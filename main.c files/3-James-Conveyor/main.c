/* CAN Receiver
*  THIS CONVEYOR
*  Poll CAN 1 every 20ms and toggle interface LED D1 on reception
*
*/

#include <stdbool.h>
#include <ucos_ii.h>
#include <bsp.h>
#include <osutils.h>
#include <can.h>
#include <leds.h>
#include <lcd.h>
#include <interface.h>
#include <conveyor.h>
#include <buttons.h>
#include <time.h>
#include <constants.h>

uint32_t pad1Status;
uint32_t robot1Status;
uint32_t cnvS1Status;
uint32_t beltStatus;
uint32_t shutdown;
uint32_t cnvS2Status;
uint32_t pad2Status;
uint32_t robot2Status;
uint32_t conv1Status = 0;
uint32_t conv2Status = 0;

/*************************************************************************
*                  PRIORITIES
*************************************************************************/

enum { 
APP_TASK_BROADCAST_PRIO = 4,  
APP_TASK_CAN_RECEIVE_PRIO,
APP_TASK_CONVEYOR_PRIO 

};

enum {
  conv1Present = 4,
  conv1Empty,
};

enum{
  conv2Present = 4,
  conv2Empty
};
/*************************************************************************
*                  APPLICATION TASK STACKS
*************************************************************************/

enum {APP_TASK_CAN_RECEIVE_STK_SIZE = 256};
enum {APP_TASK_BROADCAST_STK_SIZE = 256};
enum {APP_TASK_CONVEYOR_STK_SIZE = 256};


static OS_STK appTaskCanReceiveStk[APP_TASK_CAN_RECEIVE_STK_SIZE];
static OS_STK appTaskBroadcastStk[APP_TASK_BROADCAST_STK_SIZE];
static OS_STK appTaskConveyorStk[APP_TASK_CONVEYOR_STK_SIZE];

/*************************************************************************
*                  APPLICATION FUNCTION PROTOTYPES
*************************************************************************/

static void appTaskCanReceive(void *pdata);
static void appTaskBroadcast(void *pdata);
static void appTaskConveyor(void *pdata);

static OS_EVENT *PauseSem;
bool pause = false;
bool conveyActive = false;
bool active = false;
bool start;
bool conveyorInitialised = false;
uint8_t PauseStatus;

/*************************************************************************
*                    GLOBAL FUNCTION DEFINITIONS
*************************************************************************/


int main() {
  /* Initialise the hardware */
  bspInit();
  interfaceInit(NO_DEVICE);
  conveyorInit();
  /* Initialise the OS */
  OSInit();                                                   
  
  /* Create Tasks */
  OSTaskCreate(appTaskCanReceive,                               
               (void *)0,
               (OS_STK *)&appTaskCanReceiveStk[APP_TASK_CAN_RECEIVE_STK_SIZE - 1],
               APP_TASK_CAN_RECEIVE_PRIO);
  OSTaskCreate(appTaskBroadcast,                               
               (void *)0,
               (OS_STK *)&appTaskBroadcastStk[APP_TASK_BROADCAST_STK_SIZE - 1],
               APP_TASK_BROADCAST_PRIO);
  OSTaskCreate(appTaskConveyor,                               
               (void *)0,
               (OS_STK *)&appTaskConveyorStk[APP_TASK_CONVEYOR_STK_SIZE - 1],
               APP_TASK_CONVEYOR_PRIO);
  
  PauseSem = OSSemCreate(1);
  /* Start the OS */
  OSStart();                                                  
  
  /* Should never arrive here */ 
  return 0;      
}

/*************************************************************************
*                   APPLICATION TASK DEFINITIONS
*************************************************************************/

static void appTaskBroadcast(void *pdata) {
  canMessage_t msg = {0, 0, 0, 0};
  // bool txOk = false;
  /* Start the OS ticker
  * (must be done in the highest priority task)
  */
  /* Initialise the CAN message structure */
  // msg.id = 3;  // arbitrary CAN message id
  // msg.len = 5;    // data length 5
  
  /* 
  * Now execute the main task loop for this task
  */     
  while ( true ) {
    lcdSetTextPos(2, 4);
    lcdWrite("Active: %d", active);
    if(active){
      /*if(start){
      msg.id = convStart;
      canWrite(CAN_PORT_1, &msg);
      start = false;
      }*/ if(conveyorInitialised){
        msg.id = conveyorActive;
        canWrite(CAN_PORT_1, &msg);
        lcdSetTextPos(2, 9);
        lcdWrite("Converyor Active");
      }
      if(pause){
        msg.id = conveyorPaused;
        canWrite(CAN_PORT_1, &msg);
      }
      // Transmit message on CAN 1
      if(conv1Status == conv1Present){
        msg.id = convSensor2Active;
        canWrite(CAN_PORT_1, &msg);
      } else if(conv1Status == conv1Empty){
        msg.id = convSensor2Empty;
        canWrite(CAN_PORT_1, &msg);
      }
      lcdSetTextPos(2, 1);
      lcdWrite("Conv1: %d",msg.id);
      
      if(conv2Status == conv2Present){
         msg.id = convSensor1Active;
        canWrite(CAN_PORT_1, &msg);
      } else if(conv2Status == conv1Empty){
        msg.id = convSensor1Empty;
        canWrite(CAN_PORT_1, &msg);
      }
      lcdSetTextPos(2, 2);
      lcdWrite("Conv2: %d", msg.id);  
    
      
      /*if (txOk) {
      msg.dataA = conveyorItemPresent(CONVEYOR_SENSOR_2);
      msg.dataB = conveyorItemPresent(CONVEYOR_SENSOR_1);
    }*/
      //canWrite(CAN_PORT_1, &msg);
      
      
      lcdSetTextPos(2, 3);
      lcdWrite("------------------");
    }
    OSTimeDly(165);
  }
}

static void appTaskConveyor(void *pdata) {
  
  while(true){
    canMessage_t msg = {0, 0, 0, 0};
    msg.id = convReady;
    canWrite(CAN_PORT_1, &msg);
    lcdSetTextPos(2, 5);
    lcdWrite("Active: %d", active);
    // if(messagefrompeterabout ack)
    //start the
    if(active){
      //normal Operation
      if(conveyorItemPresent(CONVEYOR_SENSOR_1)){
        conv1Status = conv1Present;
      } else if(!conveyorItemPresent(CONVEYOR_SENSOR_1)){
        conv1Status = conv1Empty;
      }
      
      if(conveyorItemPresent(CONVEYOR_SENSOR_2)){
        conv2Status = conv2Present;
        
                
        //conveyorSetState(CONVEYOR_OFF);
      }else if(!conveyorItemPresent(CONVEYOR_SENSOR_2)){
        conv2Status = conv2Empty;
      }
      
      if(conveyorItemPresent(CONVEYOR_SENSOR_2) && (!conveyorItemPresent(CONVEYOR_SENSOR_1))){
        OSTimeDly(3000);
        conveyorSetState(CONVEYOR_REVERSE);
          //start stopclock
          //print stopclock value to LCD
      }
      
      if(conveyorGetState() == CONVEYOR_REVERSE){
        while(conveyorItemPresent(CONVEYOR_SENSOR_1)){
          /*if(stopclock > 7){
            conveyorSetState(CONVEYOR_OFF);
          }*/
             
          if(!pause){
            conveyorSetState(CONVEYOR_OFF);
          }else{
            conveyorSetState(CONVEYOR_OFF);
            OSSemPend(PauseSem, 0, &PauseStatus);
          }
          OSTimeDly(100);
        }
      }
    }
    OSTimeDly(200);
  }
}




static void appTaskCanReceive(void *pdata) {
  canMessage_t rxMsg; //init can
  osStartTick(); //start OS
  
  //main loop
  while(true){ 
    // If CAN1 is ready
    if (canReady(CAN_PORT_1)) {  
      //read can messages and update local vars
      canRead(CAN_PORT_1, &rxMsg);
      
      if(rxMsg.id== systemStart){
        lcdSetTextPos(2, 8);
        lcdWrite("System Active");
        active = true;
        conveyorInitialised = true;
      }
      
      if(rxMsg.id== emergencyShutdown){
        pad1Status = rxMsg.dataA;
        pad2Status = rxMsg.dataB;
        lcdSetTextPos(2, 4);
        lcdWrite("Emergency Shutdown Active");
        
        if(pad2Status == 1){
          interfaceLedSetState(D1_LED, LED_ON);
        }else{
          interfaceLedSetState(D1_LED, LED_OFF);
        }
      }
      if(rxMsg.id== pauseSystem){
        pause = true;
      }
      
      if(rxMsg.id==unPauseSystem){
        PauseStatus = OSSemPost(PauseSem);
        pause = false;       
      }
      
      
      
      /*    
      lcdSetTextPos(2, 2);
      lcdWrite("PAD1: %02d", pad1Status);
      lcdSetTextPos(2, 3);
      lcdWrite("ROB1: %02d", robot1Status);
      lcdSetTextPos(2, 4);
      lcdWrite("CNV1: %02d", cnvS1Status);
      lcdSetTextPos(2, 5);
      lcdWrite("CNV2: %02d", cnvS2Status);
      lcdSetTextPos(2, 6);
      lcdWrite("ROB2: %02d", robot2Status);*/
      lcdSetTextPos(2, 7);
      lcdWrite("Pause: %02d", pause);
      
    }
    OSTimeDly(100);
  }
}
