/* CAN Receiver
*
* Poll CAN 1 every 20ms and toggle interface LED D1 on reception
*
*/

#include <stdbool.h>
#include <ucos_ii.h>
#include <bsp.h>
#include <osutils.h>
#include <can.h>
#include <leds.h>
#include <lcd.h>
#include <interface.h>
#include <robot.h>
#include <buttons.h>
#include <constants.h>
#include <Robot1.h>

/*************************************************************************
*                  PRIORITIES
*************************************************************************/

enum {
  APP_TASK_CAN_SEND_PRIO = 4,
  APP_TASK_CAN_RECEIVE_PRIO,
  APP_TASK_ROBOT_PRIO
};

/*************************************************************************
*                  APPLICATION TASK STACKS
*************************************************************************/

enum {APP_TASK_CAN_RECEIVE_STK_SIZE = 256};
enum {APP_TASK_CAN_SEND_STK_SIZE = 256};
enum {APP_TASK_ROBOT_STK_SIZE = 256};

static OS_STK appTaskCanReceiveStk[APP_TASK_CAN_RECEIVE_STK_SIZE];
static OS_STK appTaskCanSendStk[APP_TASK_CAN_SEND_STK_SIZE];
static OS_STK appTaskRobotStk[APP_TASK_ROBOT_STK_SIZE];
/*************************************************************************
*                  APPLICATION FUNCTION PROTOTYPES
*************************************************************************/

static void appTaskCanReceive(void *pdata);
static void appTaskCanSend(void *pdata);
static void appTaskRobot(void *pdata);
static OS_EVENT *PauseSem;



//local vars from globals
uint32_t pad1Status     = 0;
uint32_t robot1Status   = 0;
uint32_t cnvS1Status    = 0;
uint32_t beltStatus     = 0;
uint32_t shutdown       = 0;
bool pause          = false;

//For this Board
uint32_t cnvS2Status    = 0;
uint32_t pad2Status     = 0;
uint32_t robot2Status   = 0;
uint32_t pollStatus     = 0;

/*
const uint32_t IDLE = 0;
const uint32_t READY_TO_PICKUP = 1;
const uint32_t MOVING = 2;
const uint32_t READY_TO_DROPOFF = 3;
const uint32_t SHUTDOWN = 4;
const uint32_t PAUSE = 5;

*/

uint8_t PauseStatus;


/*************************************************************************
*                    GLOBAL FUNCTION DEFINITIONS
*************************************************************************/

int main() {
  /* Initialise the hardware */
  bspInit();
  
  interfaceInit(NO_DEVICE);
  robotInit();
  
  /* Initialise the OS */
  OSInit();                                                   
  
  /* Create Tasks */
  OSTaskCreate(appTaskCanReceive,                               
               (void *)0,
               (OS_STK *)&appTaskCanReceiveStk[APP_TASK_CAN_RECEIVE_STK_SIZE - 1],
               APP_TASK_CAN_RECEIVE_PRIO);
  
  OSTaskCreate(appTaskCanSend,                               
               (void *)0,
               (OS_STK *)&appTaskCanSendStk[APP_TASK_CAN_SEND_STK_SIZE - 1],
               APP_TASK_CAN_SEND_PRIO);
  
  OSTaskCreate(appTaskRobot,                               
               (void *)0,
               (OS_STK *)&appTaskRobotStk[APP_TASK_ROBOT_STK_SIZE - 1],
               APP_TASK_ROBOT_PRIO);
  
  //semaphore for pause
  PauseSem = OSSemCreate(1);
  
  
  /* Start the OS */
  OSStart();                                                  
  
  /* Should never arrive here */ 
  return 0;      
}

/*************************************************************************
*                   APPLICATION TASK DEFINITIONS
*************************************************************************/


static void appTaskRobot(void *pdata) {
  while(true){ //main loop
    uint32_t pollStatus = 0;
    
    /********* Normal running phase  *********/
    
    if(shutdown == 0){
      robot1Status = IDLE;
      
      //Open hand
      while((robotJointGetState(ROBOT_HAND)) > RDY_HAND){
        
        if(pause){
          OSSemPend(PauseSem, 0, &PauseStatus);
        } else {
          robotJointSetState(ROBOT_HAND, ROBOT_JOINT_POS_DEC);
          OSTimeDly(10);
        }
      }
      
      
      // Move laterally to pad1 for the block
      while((robotJointGetState(ROBOT_WAIST)) < RDY_WAIST){
        if(pause){
          OSSemPend(PauseSem, 0, &PauseStatus);
        } else {
          robotJointSetState(ROBOT_WAIST, ROBOT_JOINT_POS_INC);
          OSTimeDly(10);
        }
      }  
      
      //move wrist to ready to pickup
      while((robotJointGetState(ROBOT_WRIST)) < RDY_WRIST){
        if(pause){
          OSSemPend(PauseSem, 0, &PauseStatus);
        } else {
          robotJointSetState(ROBOT_WRIST, ROBOT_JOINT_POS_INC);
          OSTimeDly(10);
        }
      }
    } 
    
    robot1Status = READY_TO_PICKUP;
    
    
    /********* If pause is pressed ********
    if(pause == 1){
    
    //move elbow to ready
    while((robotJointGetState(ROBOT_ELBOW)) < 90000){
    robotJointSetState(ROBOT_ELBOW, ROBOT_JOINT_POS_INC);
    OSTimeDly(10);
  }
    
    //open hand
    while((robotJointGetState(ROBOT_HAND)) > 45000){
    robotJointSetState(ROBOT_HAND, ROBOT_JOINT_POS_DEC);
    OSTimeDly(10);
  }
    
    // Move laterally to conveyor
    while((robotJointGetState(ROBOT_WAIST)) < 85000){
    robotJointSetState(ROBOT_WAIST, ROBOT_JOINT_POS_INC);
    OSTimeDly(10);
  }  
    
    //move wrist to ready to pickup
    while((robotJointGetState(ROBOT_WRIST)) < 90000){
    robotJointSetState(ROBOT_WRIST, ROBOT_JOINT_POS_INC);
    OSTimeDly(10);
  }  
    
    robot1Status = PAUSE;
  } */
    
    
    /********* If block on pad1  *********/
    
    if(pad1Status == 1){
      
      //Set robot1Status to moving
      robot1Status= MOVING;
      
      //go down to pick up
      if(pause == 0 && shutdown == 0){
        while((robotJointGetState(ROBOT_ELBOW)) < PU_ELBOW){
          if(pause){
            OSSemPend(PauseSem, 0, &PauseStatus);
          } else {
            robotJointSetState(ROBOT_ELBOW, ROBOT_JOINT_POS_INC);
            OSTimeDly(10);
          }
        } 
      }
      
      //Close hand
      if(pause == 0 && shutdown == 0){
        while((robotJointGetState(ROBOT_HAND)) < PU_HAND){
          if(pause){
            OSSemPend(PauseSem, 0, &PauseStatus);
          } else {
            robotJointSetState(ROBOT_HAND, ROBOT_JOINT_POS_INC);
            OSTimeDly(10);
          }
        }
      }
      
      //Move up slightly to clear pad
      if(pause == 0 && shutdown == 0){
        while((robotJointGetState(ROBOT_ELBOW)) > PDO_ELBOW){
          if(pause){
            OSSemPend(PauseSem, 0, &PauseStatus);
          } else {
            robotJointSetState(ROBOT_ELBOW, ROBOT_JOINT_POS_DEC);
            OSTimeDly(10);
          }
        } 
      }
      //move laterally to PRE-dropoff position
      if(pause == 0 && shutdown == 0){
        while((robotJointGetState(ROBOT_WAIST)) > PDO_WAIST){
          if(pause){
            OSSemPend(PauseSem, 0, &PauseStatus);
          } else {
            robotJointSetState(ROBOT_WAIST, ROBOT_JOINT_POS_DEC);
            OSTimeDly(10);
          }
        }  
      }
      //Set robot1Status to ready to dropoff
      robot1Status= READY_TO_DROPOFF;
      
      //wait X seconds for 
      pollStatus=0;
      uint32_t pollThreshold = 10;
      while(pollStatus < pollThreshold){
        //dropoff when nothing on pad
        if(cnvS1Status == 0){
          //GO DOWN
          if(pause == 0 && shutdown == 0){
            while((robotJointGetState(ROBOT_ELBOW)) < DO_ELBOW){
              if(pause){
                OSSemPend(PauseSem, 0, &PauseStatus);
              } else {
                robotJointSetState(ROBOT_ELBOW, ROBOT_JOINT_POS_INC);
                OSTimeDly(5);
              }
            } 
          }
          //Open hand
          if(pause == 0 && shutdown == 0){
            while((robotJointGetState(ROBOT_HAND)) > DO_HAND){
              if(pause){
                OSSemPend(PauseSem, 0, &PauseStatus);
              } else {
                robotJointSetState(ROBOT_HAND, ROBOT_JOINT_POS_DEC);
                OSTimeDly(5);
              }
            }
          }
          //Move up slightly to clear pad
          if(pause == 0 && shutdown == 0){
            while((robotJointGetState(ROBOT_ELBOW)) > ADO_ELBOW){
              if(pause){
                OSSemPend(PauseSem, 0, &PauseStatus);
              } else {
                robotJointSetState(ROBOT_ELBOW, ROBOT_JOINT_POS_DEC);
                OSTimeDly(5);
              }
            }
          }
          //update robot status
          robot1Status= 3;
          //get out of loop
          pollStatus =100;
        }else if(cnvS1Status!=0 && pollStatus >=9){
          //emergencyShutdown
          canMessage_t msg = {0, 0, 0, 0};
          msg.id = emergencyShutdown;
          canWrite(CAN_PORT_1, &msg);
          shutdown = 1;
        }
        
        robot1Status= MOVING;
        OSTimeDly(1000);
        pollStatus++;
      }
      
    }
    OSTimeDly(1000);
  }
  
}

static void appTaskCanReceive(void *pdata) {
  canMessage_t rxMsg; //init can
  
  
  //main loop
  while ( true ) {
    
    // Try to receive message on CAN 1
      
      interfaceLedToggle(D1_LED);
      

      //button pressed BUT1
      if(isButtonPressed(BUT_1)){ 
        lcdSetTextPos(2,7);
        lcdWrite("CNV1 Empty  ");  
        cnvS1Status = 0;
      }
      
      //button pressed BUT2
      if(isButtonPressed(BUT_2)){
        lcdSetTextPos(2,7);
        lcdWrite("CNV1 Active");  
        cnvS1Status = 1;
      }
      
      //button pressed UP
      if(isButtonPressed(JS_UP)){
        lcdSetTextPos(2,6);
        lcdWrite("PAD1 Active");  
        pad1Status = 1;
      } 
      
      
      //button pressed DOWN
      if(isButtonPressed(JS_DOWN)) {
        lcdSetTextPos(2,6);
        lcdWrite("PAD1 Empty ");  
        pad1Status = 0;
      } 
      

    OSTimeDly(100);
  }
}


static void appTaskCanSend(void *pdata) {
  osStartTick(); //start OS
  //init can msg struct
  canMessage_t msg = {0, 0, 0, 0};
  
  //main loop
  while ( true ) {
    //moving
    
    if(robot1Status == PAUSE){
      msg.id = robot1Pause;
      canWrite(CAN_PORT_1, &msg);
    }
    
    OSTimeDly(500);
  }
}