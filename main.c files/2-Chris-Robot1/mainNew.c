/* Robot-1 Subsystem
*  Christopher Hayhurst
*  Last updated: 29/4/16
*/

#include <stdbool.h>
#include <ucos_ii.h>
#include <bsp.h>
#include <osutils.h>
#include <can.h>
#include <leds.h>
#include <lcd.h>
#include <interface.h>
#include <robot.h>
#include <buttons.h>
#include <constants.h>
#include <Robot1.h>


/*************************************************************************
*                  PRIORITIES
*************************************************************************/

enum {
  APP_TASK_CAN_RECEIVE_PRIO =4,
  APP_TASK_CAN_SEND_PRIO,
  APP_TASK_ROBOT_PRIO
};

/*************************************************************************
*                  APPLICATION TASK STACKS
*************************************************************************/

enum {APP_TASK_CAN_RECEIVE_STK_SIZE = 256};
enum {APP_TASK_CAN_SEND_STK_SIZE = 256};
enum {APP_TASK_ROBOT_STK_SIZE = 256};

static OS_STK appTaskCanReceiveStk[APP_TASK_CAN_RECEIVE_STK_SIZE];
static OS_STK appTaskCanSendStk[APP_TASK_CAN_SEND_STK_SIZE];
static OS_STK appTaskRobotStk[APP_TASK_ROBOT_STK_SIZE];
/*************************************************************************
*                  APPLICATION FUNCTION PROTOTYPES
*************************************************************************/

static void appTaskCanReceive(void *pdata);
static void appTaskCanSend(void *pdata);
static void appTaskRobot(void *pdata);
static OS_EVENT *PauseSem;



// Local vars from globals
uint32_t pad1Status     = 0;
uint32_t robot1Status;
uint32_t cnvS1Status    = 0;
uint32_t beltStatus     = 0;
uint32_t shutdown       = 0;
bool pause          = false;

// For this Board
uint32_t cnvS2Status    = 0;
uint32_t pad2Status     = 0;
uint32_t robot2Status   = 0;
uint32_t pollStatus     = 0;

// Robot state variables

uint8_t PauseStatus;
bool active = false;
bool start;

uint32_t r1timeDelay = 10;

/*************************************************************************
*                    GLOBAL FUNCTION DEFINITIONS
*************************************************************************/

int main() {
  /* Initialise the hardware */
  bspInit();
  
  interfaceInit(NO_DEVICE);
  robotInit();
  
  /* Initialise the OS */
  OSInit();                                                   
  
  /* Create Tasks */
  OSTaskCreate(appTaskCanReceive,                               
               (void *)0,
               (OS_STK *)&appTaskCanReceiveStk[APP_TASK_CAN_RECEIVE_STK_SIZE - 1],
               APP_TASK_CAN_RECEIVE_PRIO);
  
  OSTaskCreate(appTaskCanSend,                               
               (void *)0,
               (OS_STK *)&appTaskCanSendStk[APP_TASK_CAN_SEND_STK_SIZE - 1],
               APP_TASK_CAN_SEND_PRIO);
  
  OSTaskCreate(appTaskRobot,                               
               (void *)0,
               (OS_STK *)&appTaskRobotStk[APP_TASK_ROBOT_STK_SIZE - 1],
               APP_TASK_ROBOT_PRIO);
  
  /* Semaphore for pause */
  PauseSem = OSSemCreate(1);
  
  
  /* Start the OS */
  OSStart();                                                  
  
  /* Should never arrive here */ 
  return 0;      
}

/*************************************************************************
*                   APPLICATION TASK DEFINITIONS
*************************************************************************/


static void appTaskRobot(void *pdata) {
  
  
  
  while(true){ //main loop
    
    /********* Normal running phase  *********/
    if (active) {
    if(shutdown == 0){
      
      if(robot1Status == IDLE){
        
        // Ready Status: Open hand 
        while((robotJointGetState(ROBOT_HAND)) > RDY_HAND){
          
          if(pause){
            OSSemPend(PauseSem, 0, &PauseStatus);
          } else {
            robotJointSetState(ROBOT_HAND, ROBOT_JOINT_POS_DEC);
            OSTimeDly(r1timeDelay);
          }
        }
        
        // Ready Status: Move waist for the pickup
        while((robotJointGetState(ROBOT_WAIST)) < RDY_WAIST){
          if(pause){
            OSSemPend(PauseSem, 0, &PauseStatus);
          } else {
            robotJointSetState(ROBOT_WAIST, ROBOT_JOINT_POS_INC);
            OSTimeDly(r1timeDelay);
          }
        }  
        
        // Ready Status: Move wrist for the pickup
        while((robotJointGetState(ROBOT_WRIST)) < RDY_WRIST){
          if(pause){
            OSSemPend(PauseSem, 0, &PauseStatus);
          } else {
            robotJointSetState(ROBOT_WRIST, ROBOT_JOINT_POS_INC);
            OSTimeDly(r1timeDelay);
          }
        }
        robot1Status = READY_TO_PICKUP;
      } 
    }
    
    
    /********* If block on pad1  *********/
    
    if(robot1Status == READY_TO_PICKUP){
      if(pad1Status == 1){
   
        if(pause == 0 && shutdown == 0){
          
          // Pick up: Move elbow down to pick up
          while((robotJointGetState(ROBOT_ELBOW)) < PU_ELBOW){
            if(pause){
              OSSemPend(PauseSem, 0, &PauseStatus);
            } else {
              robotJointSetState(ROBOT_ELBOW, ROBOT_JOINT_POS_INC);
              OSTimeDly(r1timeDelay);
            }
          } 
        }
        
        if(pause == 0 && shutdown == 0){
          
          // Pick up: Close hand to pick up block
          while((robotJointGetState(ROBOT_HAND)) < PU_HAND){
            if(pause){
              OSSemPend(PauseSem, 0, &PauseStatus);
            } else {
              robotJointSetState(ROBOT_HAND, ROBOT_JOINT_POS_INC);
              OSTimeDly(r1timeDelay);
            }
          }
        }
        
        if(pause == 0 && shutdown == 0){
          
          // Pick up: Move elbow up with the block in the hand
          while((robotJointGetState(ROBOT_ELBOW)) > PDO_ELBOW){
            if(pause){
              OSSemPend(PauseSem, 0, &PauseStatus);
            } else {
              robotJointSetState(ROBOT_ELBOW, ROBOT_JOINT_POS_DEC);
              OSTimeDly(r1timeDelay);
            }
          } 
        }
        robot1Status = MOVING;
      }
      
    }
    
    if(robot1Status == MOVING){
      if(pause == 0 && shutdown == 0){
        
        // Drop Off: Move waist over to the conveyor belt
        while((robotJointGetState(ROBOT_WAIST)) > PDO_WAIST){
          if(pause){
            OSSemPend(PauseSem, 0, &PauseStatus);
          } else {
            robotJointSetState(ROBOT_WAIST, ROBOT_JOINT_POS_DEC);
            OSTimeDly(r1timeDelay);
          }
        }  
      }
      //Set robot1Status to ready to dropoff
      robot1Status= READY_TO_DROPOFF;
    }
    
    uint32_t pollThreshold = 5;
    if((robot1Status == READY_TO_DROPOFF) && (cnvS1Status == 0)) {
      
      if(pause == 0 && shutdown == 0){
        
        // Drop Off: Move elbow down to the conveyor belt to drop off block
        while((robotJointGetState(ROBOT_ELBOW)) < DO_ELBOW){
          if(pause){
            OSSemPend(PauseSem, 0, &PauseStatus);
          } else {
            robotJointSetState(ROBOT_ELBOW, ROBOT_JOINT_POS_INC);
            OSTimeDly(r1timeDelay);
          }
        } 
      }

      if(pause == 0 && shutdown == 0){
        
        // Drop Off: Open the hand to release the block on conveyor
        while((robotJointGetState(ROBOT_HAND)) > DO_HAND){
          if(pause){
            OSSemPend(PauseSem, 0, &PauseStatus);
          } else {
            robotJointSetState(ROBOT_HAND, ROBOT_JOINT_POS_DEC);
            OSTimeDly(r1timeDelay);
          }
        }
      }

      if(pause == 0 && shutdown == 0){
        
        // After Drop Off: Move elbow up after the drop off
        while((robotJointGetState(ROBOT_ELBOW)) > ADO_ELBOW){
          if(pause){
            OSSemPend(PauseSem, 0, &PauseStatus);
          } else {
            robotJointSetState(ROBOT_ELBOW, ROBOT_JOINT_POS_DEC);
            OSTimeDly(r1timeDelay);
          }
        }
      }
      
      //update robot status
      robot1Status= IDLE;
      pollStatus = 0;
    } else if((robot1Status == READY_TO_DROPOFF) && (cnvS1Status == 1)){
      OSTimeDly(2500);
      pollStatus++;
      lcdSetTextPos(1, 5);
      lcdWrite("Poll: %d", pollStatus);
    }
    
    //  If polling threshold is exceed, shutdown the system
    
    if(pollStatus > pollThreshold){
          shutdown = 1;
    }
    
    OSTimeDly(100);
  }
  
}
}

static void appTaskCanReceive(void *pdata) {
  canMessage_t rxMsg; //init can
  
  //main loop
  while(true){ 
    
    // Check active message from control panel
    lcdSetTextPos(1, 7);
    lcdWrite("Active: %d", active);
    if (canReady(CAN_PORT_1)) {  
      
      canRead(CAN_PORT_1, &rxMsg);
      lcdSetTextPos(1, 6);
      lcdWrite("MsgID: %d", rxMsg.id);
      
      if(rxMsg.id==systemStart){
        active = true;
        robot1Status = IDLE;
      }
      if(rxMsg.id==emergencyShutdown){
        shutdown = 1;
      }
      if(rxMsg.id==pauseSystem){
        pause = true;
      }
      if(rxMsg.id==unPauseSystem){
        pause = false;
        PauseStatus = OSSemPost(PauseSem);
      }
      if(rxMsg.id==pad1Active){
        pad1Status = 1;
      }
      if(rxMsg.id==pad1Empty){
        pad1Status = 0;
      }
      if(rxMsg.id==convSensor1Active){
        cnvS1Status = 1;
      }
      if(rxMsg.id==convSensor1Empty){
        cnvS1Status = 0;
      }
      if(rxMsg.id==convSensor2Active){
        cnvS2Status = 1;
        lcdSetTextPos(2,9);
        lcdWrite("C22 %d:", rxMsg.id);
      }
      if(rxMsg.id==convSensor2Empty){
        cnvS2Status = 0;
      }
      lcdSetTextPos(1, 1);
      lcdWrite("Shutdown: %02d", shutdown);
      lcdSetTextPos(1, 2);
      lcdWrite("Pause: %02d", pause);
      lcdSetTextPos(1, 3);
      lcdWrite("Pad1: %02d", pad1Status);
      lcdSetTextPos(1, 4);
      lcdWrite("CNV1: %02d", cnvS1Status);
      lcdSetTextPos(1, 4);
      lcdWrite("Delay: %02d", r1timeDelay);
      
      
    }
    OSTimeDly(100);
  }
}


static void appTaskCanSend(void *pdata) {
  osStartTick(); //start OS
  
  //init can msg struct
  canMessage_t msg = {0, 0, 0, 0};
  
  while ( true ) {
    
        if (isButtonPressed(BUT_2)){
    r1timeDelay++;
    }
     if (isButtonPressed(BUT_1)){
    r1timeDelay--;
    }
    
    if(robot1Status == READY_TO_PICKUP){
      msg.id = robot1ReadyToPickup;
      canWrite(CAN_PORT_1, &msg);
    }
    else if(robot1Status == MOVING){
      msg.id = robot1Moving;
      canWrite(CAN_PORT_1, &msg);
    }
    else if(robot1Status == READY_TO_DROPOFF){
      msg.id = robot1ReadyToDropoff;
      canWrite(CAN_PORT_1, &msg);
    }
    else if(robot1Status == SHUTDOWN){
      msg.id = robot1Shutdown;
      canWrite(CAN_PORT_1, &msg);
      lcdSetTextPos(1, 7);
      lcdWrite("Shutdown");
    }
    else if (shutdown == 1){
      msg.id = emergencyShutdown;
      canWrite(CAN_PORT_1, &msg);    
    }
    else if(robot1Status == PAUSE){
      msg.id = robot1Pause;
      canWrite(CAN_PORT_1, &msg);
    }
    OSTimeDly(200);
  }
}