/* CAN Receiver
*
* Poll CAN 1 every 20ms and toggle interface LED D1 on reception
*
*/

#include <stdbool.h>
#include <ucos_ii.h>
#include <bsp.h>
#include <osutils.h>
#include <can.h>
#include <leds.h>
#include <lcd.h>
#include <interface.h>
#include <robot.h>
#include <buttons.h>
#include <constants.h>
#include <Robot1.h>

/*************************************************************************
*                  PRIORITIES
*************************************************************************/

enum {
  APP_TASK_CAN_RECEIVE_PRIO =4,
  APP_TASK_CAN_SEND_PRIO,
  APP_TASK_ROBOT_PRIO
};

/*************************************************************************
*                  APPLICATION TASK STACKS
*************************************************************************/

enum {APP_TASK_CAN_RECEIVE_STK_SIZE = 256};
enum {APP_TASK_CAN_SEND_STK_SIZE = 256};
enum {APP_TASK_ROBOT_STK_SIZE = 256};

static OS_STK appTaskCanReceiveStk[APP_TASK_CAN_RECEIVE_STK_SIZE];
static OS_STK appTaskCanSendStk[APP_TASK_CAN_SEND_STK_SIZE];
static OS_STK appTaskRobotStk[APP_TASK_ROBOT_STK_SIZE];

/*************************************************************************
*                  APPLICATION FUNCTION PROTOTYPES
*************************************************************************/

static void appTaskCanReceive(void *pdata);
static void appTaskCanSend(void *pdata);
static void appTaskRobot(void *pdata);
static OS_EVENT *PauseSem;

//local vars from globals
uint32_t pad1Status     = 0;
uint32_t robot1Status   = 0;
uint32_t cnvS1Status    = 0;
uint32_t beltStatus     = 0;
uint32_t shutdown       = 0;
bool active = false;
bool pause          = false;
bool start;
//For this Board
uint32_t cnvS2Status    = 0;
uint32_t pad2Status     = 0;
uint32_t robot2Status   = 0;
uint32_t pollStatus     = 0;
uint8_t PauseStatus;


/*************************************************************************
*                    GLOBAL FUNCTION DEFINITIONS
*************************************************************************/

int main() {
  /* Initialise the hardware */
  bspInit();
  
  interfaceInit(NO_DEVICE);
  robotInit();
  
  /* Initialise the OS */
  OSInit();                                                   
  
  /* Create Tasks */
  OSTaskCreate(appTaskCanReceive,                               
               (void *)0,
               (OS_STK *)&appTaskCanReceiveStk[APP_TASK_CAN_RECEIVE_STK_SIZE - 1],
               APP_TASK_CAN_RECEIVE_PRIO);
  
  OSTaskCreate(appTaskCanSend,                               
               (void *)0,
               (OS_STK *)&appTaskCanSendStk[APP_TASK_CAN_SEND_STK_SIZE - 1],
               APP_TASK_CAN_SEND_PRIO);
  
  OSTaskCreate(appTaskRobot,                               
               (void *)0,
               (OS_STK *)&appTaskRobotStk[APP_TASK_ROBOT_STK_SIZE - 1],
               APP_TASK_ROBOT_PRIO);
  
  /* Create pause semaphore */
  PauseSem = OSSemCreate(1);
  
  /* Start the OS */
  OSStart();                                                  
  
  /* Should never arrive here */ 
  return 0;      
}

/*************************************************************************
*                   APPLICATION TASK DEFINITIONS
*************************************************************************/


static void appTaskRobot(void *pdata) {
  
  while(true){ //main loop
    canMessage_t msg = {0, 0, 0, 0};
    msg.id = robot1Ready;
    canWrite(CAN_PORT_1, &msg);
    if (active) { 
      
      uint32_t pollStatus = 0;
      
      /********* Normal running phase  *********/
      
      if(shutdown == 0){
        robot1Status = IDLE;
        
        //Open hand
        while((robotJointGetState(ROBOT_HAND)) > RDY_HAND){
          
          if(pause){
            OSSemPend(PauseSem, 0, &PauseStatus);
          } else {
            robotJointSetState(ROBOT_HAND, ROBOT_JOINT_POS_DEC);
            OSTimeDly(10);
          }
        }
        
        // Move laterally to pad1 for the block
        while((robotJointGetState(ROBOT_WAIST)) < RDY_WAIST){
          if(pause){
            OSSemPend(PauseSem, 0, &PauseStatus);
          } else {
            robotJointSetState(ROBOT_WAIST, ROBOT_JOINT_POS_INC);
            OSTimeDly(10);
          }
        }  
        
        //move wrist to ready to pickup
        while((robotJointGetState(ROBOT_WRIST)) < RDY_WRIST){
          if(pause){
            OSSemPend(PauseSem, 0, &PauseStatus);
          } else {
            robotJointSetState(ROBOT_WRIST, ROBOT_JOINT_POS_INC);
            OSTimeDly(10);
          }
        }
      } 
      
     robot1Status = READY_TO_PICKUP;
     lcdSetTextPos(1, 8);
     lcdWrite("RobotSt: %d", robot1Status);
      /********* If block on pad1  *********/
      
      if(pad1Status == 1){
        
        //Set robot1Status to moving
        robot1Status= MOVING;
        
        //go down to pick up
        if(pause == 0 && shutdown == 0){
          while((robotJointGetState(ROBOT_ELBOW)) < PU_ELBOW){
            if(pause){
              OSSemPend(PauseSem, 0, &PauseStatus);
            } else {
              robotJointSetState(ROBOT_ELBOW, ROBOT_JOINT_POS_INC);
              OSTimeDly(10);
            }
          } 
        }
        
        //Close hand
        if(pause == 0 && shutdown == 0){
          while((robotJointGetState(ROBOT_HAND)) < PU_HAND){
            if(pause){
              OSSemPend(PauseSem, 0, &PauseStatus);
            } else {
              robotJointSetState(ROBOT_HAND, ROBOT_JOINT_POS_INC);
              OSTimeDly(10);
            }
          }
        }
        
        //Move up slightly to clear pad
        if(pause == 0 && shutdown == 0){
          while((robotJointGetState(ROBOT_ELBOW)) > PDO_ELBOW){
            if(pause){
              OSSemPend(PauseSem, 0, &PauseStatus);
            } else {
              robotJointSetState(ROBOT_ELBOW, ROBOT_JOINT_POS_DEC);
              OSTimeDly(10);
            }
          } 
        }
        //move laterally to PRE-dropoff position
        if(pause == 0 && shutdown == 0){
          while((robotJointGetState(ROBOT_WAIST)) > PDO_WAIST){
            if(pause){
              OSSemPend(PauseSem, 0, &PauseStatus);
            } else {
              robotJointSetState(ROBOT_WAIST, ROBOT_JOINT_POS_DEC);
              OSTimeDly(10);
            }
          }  
        }
        //Set robot1Status to ready to dropoff
        robot1Status= READY_TO_DROPOFF;
        
        //wait X seconds for 
        pollStatus=0;
        uint32_t pollThreshold = 10;
        while(pollStatus < pollThreshold){
          //dropoff when nothing on pad
          if(cnvS1Status == 0){
            //GO DOWN
            if(pause == 0 && shutdown == 0){
              while((robotJointGetState(ROBOT_ELBOW)) < DO_ELBOW){
                if(pause){
                  OSSemPend(PauseSem, 0, &PauseStatus);
                } else {
                  robotJointSetState(ROBOT_ELBOW, ROBOT_JOINT_POS_INC);
                  OSTimeDly(5);
                }
              } 
            }
            //Open hand
            if(pause == 0 && shutdown == 0){
              while((robotJointGetState(ROBOT_HAND)) > DO_HAND){
                if(pause){
                  OSSemPend(PauseSem, 0, &PauseStatus);
                } else {
                  robotJointSetState(ROBOT_HAND, ROBOT_JOINT_POS_DEC);
                  OSTimeDly(5);
                }
              }
            }
            //Move up slightly to clear pad
            if(pause == 0 && shutdown == 0){
              while((robotJointGetState(ROBOT_ELBOW)) > ADO_ELBOW){
                if(pause){
                  OSSemPend(PauseSem, 0, &PauseStatus);
                } else {
                  robotJointSetState(ROBOT_ELBOW, ROBOT_JOINT_POS_DEC);
                  OSTimeDly(5);
                }
              }
            }
            //update robot status
            robot1Status= 3;
            //get out of loop
            pollStatus =100;
          }else if(cnvS1Status!=0 && pollStatus >=9){
            //emergencyShutdown
            shutdown = 1;
          }
          
          robot1Status= MOVING;
          OSTimeDly(1000);
          pollStatus++;
          
        }
        
      }
      OSTimeDly(100);
    }
  }
}

static void appTaskCanReceive(void *pdata) {
  canMessage_t rxMsg; //init can
  
  //main loop
  while(true){ 
    // If CAN1 is ready
    lcdSetTextPos(1, 7);
    lcdWrite("Active: %d", active);
    if (canReady(CAN_PORT_1)) {  
      //read can messages and update local vars
      canRead(CAN_PORT_1, &rxMsg);
      lcdSetTextPos(1, 6);
      lcdWrite("MsgID: %d", rxMsg.id);
      
      if(rxMsg.id==systemStart){
        active = true;
      }
      if(rxMsg.id==emergencyShutdown){
        shutdown = 1;
      }
      if(rxMsg.id==pauseSystem){
        pause = true;
      }
      if(rxMsg.id==unPauseSystem){
        pause = false;
        PauseStatus = OSSemPost(PauseSem);
      }
      if(rxMsg.id==pad1Active){
        pad1Status = 1;
      }
      if(rxMsg.id==pad1Empty){
        pad1Status = 0;
      }
      if(rxMsg.id==convSensor1Active){
        cnvS1Status = 1;
      }
      if(rxMsg.id==convSensor1Empty){
        cnvS1Status = 0;
      }
      if(rxMsg.id==convSensor2Active){
        cnvS2Status = 1;
        lcdSetTextPos(2,9);
        lcdWrite("C22 %d:", rxMsg.id);
      }
      if(rxMsg.id==convSensor2Empty){
        cnvS2Status = 0;
      }
      lcdSetTextPos(1, 1);
      lcdWrite("SD: %02d", shutdown);
      lcdSetTextPos(1, 2);
      lcdWrite("PA: %02d", pause);
      lcdSetTextPos(1, 3);
      lcdWrite("P1: %02d", pad1Status);
      lcdSetTextPos(1, 4);
      lcdWrite("C1: %02d", cnvS1Status);
      lcdSetTextPos(1, 5);
      lcdWrite("C2: %02d", cnvS2Status);
      //lcdSetTextPos(1, 6);
      //lcdWrite("R2: %02d", robot1Status);
      
    }
    OSTimeDly(100);
  }
}

static void appTaskCanSend(void *pdata) {
  osStartTick(); //start OS
  //init can msg struct
  canMessage_t msg = {0, 0, 0, 0};
  
  //main loop
  while ( true ) {
    //moving
   if(robot1Status == READY_TO_PICKUP){
      msg.id = robot1ReadyToPickup;
      canWrite(CAN_PORT_1, &msg);
      lcdSetTextPos(1, 9);
      lcdWrite("Ready to Pick");
    }
    else if(robot1Status == MOVING){
      msg.id = robot1Moving;
      canWrite(CAN_PORT_1, &msg);
    }
    else if(robot1Status == READY_TO_DROPOFF){
      msg.id = robot1ReadyToDropoff;
      canWrite(CAN_PORT_1, &msg);
    }
    else if(robot1Status == SHUTDOWN){
      msg.id = robot1Shutdown;
      canWrite(CAN_PORT_1, &msg);
    }
    else if (shutdown == 1){
      msg.id = emergencyShutdown;
     canWrite(CAN_PORT_1, &msg);    
    }
    else if(robot1Status == PAUSE){
      msg.id = robot1Pause;
      canWrite(CAN_PORT_1, &msg);
    }
    OSTimeDly(200);
  }
}