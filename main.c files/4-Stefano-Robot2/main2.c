/* 
* Robot 2 Code
* EN0617
* Industrial Case Project
* Stefano Mantini
*/

#include <stdbool.h>
#include <ucos_ii.h>
#include <bsp.h>
#include <osutils.h>
#include <can.h>
#include <leds.h>
#include <lcd.h>
#include <interface.h>
#include <robot.h>
#include <buttons.h>
#include <constants.h>
#include <Robot2.h>

/*************************************************************************
*                  PRIORITIES
*************************************************************************/

enum {
  APP_TASK_CAN_SEND_PRIO = 4,
  APP_TASK_CAN_RECEIVE_PRIO,
  APP_TASK_ROBOT_PRIO
};

/*************************************************************************
*                  APPLICATION TASK STACKS
*************************************************************************/

enum {APP_TASK_CAN_RECEIVE_STK_SIZE = 256};
enum {APP_TASK_CAN_SEND_STK_SIZE = 256};
enum {APP_TASK_ROBOT_STK_SIZE = 256};

static OS_STK appTaskCanReceiveStk[APP_TASK_CAN_RECEIVE_STK_SIZE];
static OS_STK appTaskCanSendStk[APP_TASK_CAN_SEND_STK_SIZE];
static OS_STK appTaskRobotStk[APP_TASK_ROBOT_STK_SIZE];

/*************************************************************************
*                  APPLICATION FUNCTION PROTOTYPES
*************************************************************************/

static void appTaskCanReceive(void *pdata);
static void appTaskCanSend(void *pdata);
static void appTaskRobot(void *pdata);
static OS_EVENT *PauseSem;

//local vars from globals
bool pad1Status         = false;
bool cnvS1Status        = false;
bool beltStatus         = false;
bool shutdown           = false;
bool reset              = false;
bool startStopInit      = false;
bool pause              = false;

//For this Board
bool cnvS2Status        = false;
bool pad2Status         = false;
uint8_t PauseStatus     = false;
uint32_t robot2Status   = 0;
uint32_t pollStatus     = 0;
uint32_t pollPad2       = 0;


/*************************************************************************
*                    GLOBAL FUNCTION DEFINITIONS
*************************************************************************/

int main() {
  /* Initialise the hardware */
  bspInit();
  
  //init LEDs off
  interfaceLedSetState(USB_CONNECT_LED, LED_OFF);
  interfaceLedSetState(USB_LINK_LED, LED_OFF);
  
  interfaceInit(NO_DEVICE);
  robotInit();
  
  /* Initialise the OS */
  OSInit();                                                   
  
  /* Create Tasks */
  OSTaskCreate(appTaskCanReceive,                               
               (void *)0,
               (OS_STK *)&appTaskCanReceiveStk[APP_TASK_CAN_RECEIVE_STK_SIZE - 1],
               APP_TASK_CAN_RECEIVE_PRIO);
  
  OSTaskCreate(appTaskCanSend,                               
               (void *)0,
               (OS_STK *)&appTaskCanSendStk[APP_TASK_CAN_SEND_STK_SIZE - 1],
               APP_TASK_CAN_SEND_PRIO);
  
  OSTaskCreate(appTaskRobot,                               
               (void *)0,
               (OS_STK *)&appTaskRobotStk[APP_TASK_ROBOT_STK_SIZE - 1],
               APP_TASK_ROBOT_PRIO);
  
  /* Create pause semaphore */
  PauseSem = OSSemCreate(1);
  
  /* Start the OS */
  OSStart();                                                  
  
  /* Should never arrive here */ 
  return 0;      
}

/*************************************************************************
*                   APPLICATION TASK DEFINITIONS
*************************************************************************/


static void appTaskRobot(void *pdata) {
  
  while(true){ //main loop

    /**
    * SHUTDOWN State
    * block at shutdown until reset
    */
    if(robot2Status == SHUTDOWN){
      startStopInit = false;
      lcdSetTextPos(1,3);
      lcdWrite("SHUTDOWN   ");
      if(reset){
        robot2Status = IDLE;
        startStopInit = true;
        lcdSetTextPos(1,3);
        lcdWrite("RESET    ");
      }
    }
    
    /**
    * Start/stop statement
    */
    
    if (startStopInit) {
              
      /**
      * SOP Running State
      */
      if(!shutdown){
        if(robot2Status == IDLE){
          
          //Open hand
          while((robotJointGetState(ROBOT_HAND)) > RDY_HAND){
            if(shutdown){
              robot2Status = SHUTDOWN;
            }else if(pause){
              OSSemPend(PauseSem, 0, &PauseStatus);
              robot2Status = IDLE;            
            }
            robotJointSetState(ROBOT_HAND, ROBOT_JOINT_POS_DEC);
            OSTimeDly(RbtDly);
          }
          
          // Move laterally to pad1 for the block
          while((robotJointGetState(ROBOT_WAIST)) > RDY_WAIST){
            if(shutdown){
              robot2Status = SHUTDOWN;
            }else if(pause){
              OSSemPend(PauseSem, 0, &PauseStatus);
              robot2Status = IDLE;      
            }
            robotJointSetState(ROBOT_WAIST, ROBOT_JOINT_POS_DEC);
            OSTimeDly(RbtDly);
          }  
          
          //move wrist to ready to pickup
          while((robotJointGetState(ROBOT_WRIST)) < RDY_WRIST){
            if(shutdown){
              robot2Status = SHUTDOWN;
            }else if(pause){
              OSSemPend(PauseSem, 0, &PauseStatus);
              robot2Status = IDLE;           
            }
            robotJointSetState(ROBOT_WRIST, ROBOT_JOINT_POS_INC);
            OSTimeDly(RbtDly);
          }

          robot2Status = READY_TO_PICKUP;            
          
        }
        OSTimeDly(1000);
        
        
        /**
        * Input on cnv sensor 2
        */
        uint32_t pollThreshold = 5;
        
        
        /**
        * If in ready state & cnv sensor has input 
        */
        if((cnvS2Status) && (robot2Status == READY_TO_PICKUP)){
          if(pollStatus <= pollThreshold){
            
            while((robotJointGetState(ROBOT_HAND)) > RDY_HAND){
              if(shutdown){
                robot2Status = SHUTDOWN;
              } else if(pause){
                OSSemPend(PauseSem, 0, &PauseStatus);
                robot2Status = READY_TO_PICKUP;
              }
              //HAND
              robotJointSetState(ROBOT_HAND, ROBOT_JOINT_POS_DEC);
              OSTimeDly(RbtDly);
            }
            
            
            while((robotJointGetState(ROBOT_ELBOW)) < PU_ELBOW){
              if(shutdown){
                robot2Status = SHUTDOWN;
              } else if(pause){
                OSSemPend(PauseSem, 0, &PauseStatus);
                robot2Status = READY_TO_PICKUP;
              }
                //ELBOW
                robotJointSetState(ROBOT_ELBOW, ROBOT_JOINT_POS_INC);
                OSTimeDly(RbtDly); 
            }
            
            while((robotJointGetState(ROBOT_HAND)) < PU_HAND){
              if(shutdown){
                robot2Status = SHUTDOWN;
              } else if(pause){
                OSSemPend(PauseSem, 0, &PauseStatus);
                robot2Status = READY_TO_PICKUP;
              }
              //close hand
              robotJointSetState(ROBOT_HAND, ROBOT_JOINT_POS_INC);
              OSTimeDly(RbtDly);
            }
            
            while((robotJointGetState(ROBOT_ELBOW)) > PPU_ELBOW){
              if(shutdown){
                robot2Status = SHUTDOWN;
              } else if(pause) {
                OSSemPend(PauseSem, 0, &PauseStatus);
                robot2Status = READY_TO_PICKUP;
              }
              //Move up slightly to clear pad
              robotJointSetState(ROBOT_ELBOW, ROBOT_JOINT_POS_DEC);
              OSTimeDly(RbtDly);
            }
            
            pollStatus++;
            OSTimeDly(2500);
            if(robot2Status == READY_TO_PICKUP && cnvS2Status == 0){
              robot2Status = MOVING;
            }
            
          }else{
            robot2Status = ERROR;
          }
        }
        
        /**
         * Successful Pickup
         */
          if(robot2Status == MOVING){
            
            while((robotJointGetState(ROBOT_WAIST)) < PreDO_WAIST){
              if(shutdown){
                robot2Status = SHUTDOWN;
              }else if(pause){
                OSSemPend(PauseSem, 0, &PauseStatus);
                robot2Status = MOVING;
              }
              //move laterally to dropoff
              robotJointSetState(ROBOT_WAIST, ROBOT_JOINT_POS_INC);
              OSTimeDly(RbtDly);
            }
            
            robot2Status = READY_TO_DROPOFF;
            pollStatus = 0;
          }
        
        OSTimeDly(150);
        
        if((robot2Status == READY_TO_DROPOFF) && (pad2Status == 0)){

          while((robotJointGetState(ROBOT_ELBOW)) < DO_ELBOW){
              if(shutdown){
                robot2Status = SHUTDOWN;
              }else if(pause){
                OSSemPend(PauseSem, 0, &PauseStatus);
                robot2Status = READY_TO_DROPOFF;
              }
              robotJointSetState(ROBOT_ELBOW, ROBOT_JOINT_POS_INC);
              OSTimeDly(RbtDly);
          } 

          while((robotJointGetState(ROBOT_HAND)) > DO_HAND){
            if(shutdown){
              robot2Status = SHUTDOWN;
            }else if(pause){
              OSSemPend(PauseSem, 0, &PauseStatus);
              robot2Status = READY_TO_DROPOFF;
            }
            robotJointSetState(ROBOT_HAND, ROBOT_JOINT_POS_DEC);
            OSTimeDly(RbtDly);
          }

          while((robotJointGetState(ROBOT_ELBOW)) > PostDO_ELBOW){
            if(shutdown){
              robot2Status = SHUTDOWN;
            } else if(pause){
              OSSemPend(PauseSem, 0, &PauseStatus);
              robot2Status = READY_TO_DROPOFF;
            }
            robotJointSetState(ROBOT_ELBOW, ROBOT_JOINT_POS_DEC);
            OSTimeDly(RbtDly);
          }

          robot2Status = IDLE;
          
          pollPad2 = 0;
          
        }else if((robot2Status == READY_TO_DROPOFF) && (pad2Status)){
          OSTimeDly(2500);
          pollPad2++;
        }
        if(pollPad2 > pollThreshold){
          robot2Status = ERROR;
        }
      }
      //short dly
      OSTimeDly(100);
    }
    
  }
}


static void appTaskCanReceive(void *pdata) {
  
  canMessage_t rxMsg; //init can
  //main loop
  while(true){ 
    
    /**
    * Individual Testing
    */
    //Buttons toggle statuses
    if(isButtonPressed(BUT_1)){
      startStopInit = !startStopInit;
    }
    if(isButtonPressed(BUT_2)){
      cnvS2Status = !cnvS2Status;
    }
    if(isButtonPressed(JS_CENTRE)){
      pad2Status = !pad2Status;
    }
    if(isButtonPressed(JS_UP)){
      shutdown = !shutdown;
    }
    if(isButtonPressed(JS_DOWN)){
      pause = !pause;
    }
    // END Individual testing
    
    
    /**
    * LED Debugging
    */
    //start/stop
    if(startStopInit){
      interfaceLedSetState(D1_LED, LED_ON);
    }else{
      interfaceLedSetState(D1_LED, LED_OFF);
    }
    //pause
    if(pause){
      interfaceLedSetState(D2_LED, LED_ON);
    }else{
      interfaceLedSetState(D2_LED, LED_OFF);
    }
    //conveyor 2 status
    if(cnvS2Status){
      interfaceLedSetState(USB_LINK_LED, LED_ON);
    }else{
      interfaceLedSetState(USB_LINK_LED, LED_OFF);
    }
    //pad2 status
    if(pad2Status){
      interfaceLedSetState(USB_CONNECT_LED, LED_ON);
    }else{
      interfaceLedSetState(USB_CONNECT_LED, LED_OFF);
    }
    //shutdown
    if(shutdown){
      interfaceLedSetState(D4_LED, LED_ON);
    }
    //pause
    if(reset){
      interfaceLedSetState(D4_LED, LED_OFF);
    }
    // END LED Debugging
    
    
    
    /**
    * Update local vars from globals
    */    
    if (canReady(CAN_PORT_1)) {
      canRead(CAN_PORT_1, &rxMsg);
      if(rxMsg.id==systemStart){
        startStopInit = true;
        robot2Status = IDLE;
      }
      if(rxMsg.id==systemStop){
        startStopInit = false;
      }
      if(rxMsg.id==emergencyShutdown){
        robot2Status = SHUTDOWN;
        shutdown = true;
      }
      if(rxMsg.id==resetSystem){
        robot2Status = IDLE;
        shutdown = false;
        lcdSetTextPos(1,3);
        lcdWrite("RESET    ");
      }
      if(rxMsg.id==pauseSystem){
        robot2Status = PAUSE;
        pause = true;
      }
      if(rxMsg.id==unPauseSystem){
        pause = false;
        PauseStatus = OSSemPost(PauseSem);
      }
      if(rxMsg.id==pad2Active){
        pad2Status = true;
      }
      if(rxMsg.id==pad2Empty){
        pad2Status = false;
      }
      if(rxMsg.id==convSensor1Active){
        cnvS1Status = true;
      }
      if(rxMsg.id==convSensor1Empty){
        cnvS1Status = false;
      }
      if(rxMsg.id==convSensor2Active){
        cnvS2Status = true;
      }
      if(rxMsg.id==convSensor2Empty){
        cnvS2Status = false;
      }  
    }
    //short delay
    OSTimeDly(100);
  }
}


static void appTaskCanSend(void *pdata) {
  osStartTick(); //start OS
  //init can msg
  canMessage_t msg = {0, 0, 0, 0};
  
  //main loop
  while (true) {
    
    //send on startup
    msg.id = robot2Ready;
    canWrite(CAN_PORT_1, &msg);
        
    lcdSetTextPos(1,1);
    lcdWrite("ROB2: %d", robot2Status);
    
    lcdSetTextPos(1,5);
    lcdWrite("CNV2 %d", cnvS2Status);
    lcdSetTextPos(1,6);
    lcdWrite("PAD2 %d", pad2Status);
    
    lcdSetTextPos(1,9);
    lcdWrite("PAUS %d", pause);
    lcdSetTextPos(1,10);
    lcdWrite("SHTD %d", shutdown);
    
    
    if(robot2Status == IDLE){
      msg.id = robot2Start;
      canWrite(CAN_PORT_1, &msg);
    }
    if(robot2Status == READY_TO_PICKUP){
      msg.id = robot2ReadyToPickup;
      canWrite(CAN_PORT_1, &msg);
    }
    else if(robot2Status == MOVING){
      msg.id = robot2Moving;
      canWrite(CAN_PORT_1, &msg);
    }
    else if(robot2Status == MOVINGBACK){
      msg.id = robot2Moving;
      canWrite(CAN_PORT_1, &msg);
    }
    else if(robot2Status == READY_TO_DROPOFF){
      msg.id = robot2ReadyToDropoff;
      canWrite(CAN_PORT_1, &msg);
    }
    else if(robot2Status == ERROR){
      msg.id = error;
      canWrite(CAN_PORT_1, &msg);
    }else if(robot2Status == PAUSE){
      msg.id = robot2Pause;
      canWrite(CAN_PORT_1, &msg);
    }
    
    
    OSTimeDly(500);
  }
}