# README #

Contains both the formal model and the EWARM project

### Useful Links ###

* [Drive dir](https://drive.google.com/drive/folders/0B4wDublaWjh0QUxKQjJKZ0tCX1E)
* [Module Guide](http://cgweb1.northumbria.ac.uk/SubjectAreaResources/EN617/)

### Git ###
How to commit files
`git init`
`git clone http://USERNAME@bitbucket.org/sman6798/en0617-case-study.git`
`cd to pull-ed directory cd Desktop/en617`
`copy main.c file to correct directory`
`git add pathTo/main.c file in correct directory`
`git status`
`git commit -m 'Commit Message'`
`git push u- origin master`

if origin master fails, check that you have a remote called origin 
`git remote -v` should list an origin with the repository
if not use below and try again
`git remote add origin https://USERNAME@bitbucket.org/sman6798/en0617-case-study.git`
